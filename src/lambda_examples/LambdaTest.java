package lambda_examples;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LambdaTest {

	public static void main(String[] args) {
		//ggggg
		////
		List<Integer> list = Arrays.asList(1, 9, 7, 10, 8);
		/*
		     Collections.sort(list, new Comparator<Integer>() {
			  @Override
			  public int compare(Integer i1, Integer i2) {
			    return i1.compareTo(i2);
			  }
			});
		 */
		//test
		Collections.sort(list, (i1, i2) -> i1.compareTo(i2));
		for(Integer i :list){
			System.out.println(i);
		}
	}

}
